package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 第三方模块池
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="module")
public class ModuleEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模块编码
     */
	private String code;
    /**
     * 模块名
     */
	private String name;
    /**
     * 模块说明
     */
	private String description;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return "ModuleEntity{" +
			"code=" + code +
			", name=" + name +
			", description=" + description +
			"}";
	}
}
