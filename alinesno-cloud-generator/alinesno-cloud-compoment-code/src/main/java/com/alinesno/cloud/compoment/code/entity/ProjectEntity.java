package com.alinesno.cloud.compoment.code.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 项目信息
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="project")
public class ProjectEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 项目编码
     */
	private String code;
    /**
     * 项目名
     */
	private String name;
    /**
     * 项目描述
     */
	private String description;
    /**
     * 项目英文名
     */
	private String englishName;
    /**
     * 数据库类型:Mysql,Oracle
     */
	private String databaseType;
    /**
     * 项目语言:Java,Python,Js
     */
	private String language;
    /**
     * 项目状态：停用[Disenable]，启用[Enable]
     */
	private String state;
    /**
     * 项目版权文字信息
     */
	private String copyright;
    /**
     * 作者
     */
	private String author;
    /**
     * 联系方式
     */
	private String phone;
    /**
     * 项目基础包名
     */
	private String basePackage;
    /**
     * 数据库脚本文件地址
     */
	private String sqlFile;
    /**
     * 项目下载地址
     */
	private String downloadUrl;
    /**
     * 生成次数
     */
	private Integer buildNumber;
    /**
     * 是否仓库管理
     */
	private String isRepository;
    /**
     * 是否已经解析表
     */
	private String isParseTable;
    /**
     * 是否已经解析类
     */
	private String isParseClass;
    /**
     * 创建时间
     */
	private Date createTime;
    /**
     * 账户编码
     */
	private String accountCode;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getDatabaseType() {
		return databaseType;
	}

	public void setDatabaseType(String databaseType) {
		this.databaseType = databaseType;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getBasePackage() {
		return basePackage;
	}

	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	public String getSqlFile() {
		return sqlFile;
	}

	public void setSqlFile(String sqlFile) {
		this.sqlFile = sqlFile;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public Integer getBuildNumber() {
		return buildNumber;
	}

	public void setBuildNumber(Integer buildNumber) {
		this.buildNumber = buildNumber;
	}

	public String getIsRepository() {
		return isRepository;
	}

	public void setIsRepository(String isRepository) {
		this.isRepository = isRepository;
	}

	public String getIsParseTable() {
		return isParseTable;
	}

	public void setIsParseTable(String isParseTable) {
		this.isParseTable = isParseTable;
	}

	public String getIsParseClass() {
		return isParseClass;
	}

	public void setIsParseClass(String isParseClass) {
		this.isParseClass = isParseClass;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}


	@Override
	public String toString() {
		return "ProjectEntity{" +
			"code=" + code +
			", name=" + name +
			", description=" + description +
			", englishName=" + englishName +
			", databaseType=" + databaseType +
			", language=" + language +
			", state=" + state +
			", copyright=" + copyright +
			", author=" + author +
			", phone=" + phone +
			", basePackage=" + basePackage +
			", sqlFile=" + sqlFile +
			", downloadUrl=" + downloadUrl +
			", buildNumber=" + buildNumber +
			", isRepository=" + isRepository +
			", isParseTable=" + isParseTable +
			", isParseClass=" + isParseClass +
			", createTime=" + createTime +
			", accountCode=" + accountCode +
			"}";
	}
}
