package com.alinesno.cloud.compoment.code.devops;

import com.alinesno.cloud.compoment.code.devops.tools.WSTools;
import com.alinesno.cloud.compoment.code.entity.ProjectJobEntity;


/**
 * 生成文件接口定义
 * Created by lixin on 2018/2/1.
 */
public interface GenerateCode {
    /**
     * 根据项目码创建项目代码
     * @param projectCode 项目编码
     * @param projectJob  项目job
     * @param webSocketSession  socket连接
     */
    void aiCode(String projectCode, ProjectJobEntity projectJob, WSTools webSocketSession);
}
