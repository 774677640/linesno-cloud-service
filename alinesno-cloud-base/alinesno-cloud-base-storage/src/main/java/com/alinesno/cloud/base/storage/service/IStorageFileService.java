package com.alinesno.cloud.base.storage.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.storage.entity.StorageFileEntity;
import com.alinesno.cloud.base.storage.repository.StorageFileRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
@NoRepositoryBean
public interface IStorageFileService extends IBaseService<StorageFileRepository, StorageFileEntity, String> {

}
