package com.alinesno.cloud.base.print.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-06-07 21:26:00
 */
@SuppressWarnings("serial")
public class NamespaceDto extends BaseDto {

    /**
     * 模板空间名称
     */
	private String namespace;
	
    /**
     * 模板空间排序
     */
	private Integer namespaceSort;
	
	// private Integer namespaceSort;
	


	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

//	public Integer getNamespaceSort() {
//		return namespaceSort;
//	}
//
//	public void setNamespaceSort(Integer namespaceSort) {
//		this.namespaceSort = namespaceSort;
//	}

	public Integer getNamespaceSort() {
		return namespaceSort;
	}

	public void setNamespaceSort(Integer namespaceSort) {
		this.namespaceSort = namespaceSort;
	}

}
