package com.alinesno.cloud.base.boot.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;

import com.alinesno.cloud.base.boot.feign.dto.ContentNoticeDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-04 14:20:07
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="contentNotice")
public interface ContentNoticeFeigin extends IBaseFeign<ContentNoticeDto> {

}
