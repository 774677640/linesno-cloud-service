package com.alinesno.cloud.base.boot.feign.facade;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alinesno.cloud.base.boot.feign.dto.ManagerApplicationDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:02:27
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="managerApplication")
public interface ManagerApplicationFeigin extends IBaseFeign<ManagerApplicationDto> {

	/**
	 * 根据用户查询用户的所属应用
	 * @param id
	 * @return
	 */
	@GetMapping("findAllByAccountId")
	List<ManagerApplicationDto> findAllByAccountId(@RequestParam("accountId") String accountId);

}
