package com.alinesno.cloud.base.boot.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.boot.entity.ManagerSourceGenerateEntity;
import com.alinesno.cloud.base.boot.repository.ManagerSourceGenerateRepository;
import com.alinesno.cloud.base.boot.service.IManagerSourceGenerateService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-17 07:44:37
 */
@Service
public class ManagerSourceGenerateServiceImpl extends IBaseServiceImpl<ManagerSourceGenerateRepository, ManagerSourceGenerateEntity, String> implements IManagerSourceGenerateService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ManagerSourceGenerateServiceImpl.class);

}
