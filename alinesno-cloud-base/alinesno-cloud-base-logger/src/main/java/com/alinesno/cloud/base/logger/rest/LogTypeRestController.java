package com.alinesno.cloud.base.logger.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.logger.entity.LogTypeEntity;
import com.alinesno.cloud.base.logger.service.ILogTypeService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@Scope("prototype")
@RestController
@RequestMapping("logType")
public class LogTypeRestController extends BaseRestController<LogTypeEntity , ILogTypeService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(LogTypeRestController.class);

}
