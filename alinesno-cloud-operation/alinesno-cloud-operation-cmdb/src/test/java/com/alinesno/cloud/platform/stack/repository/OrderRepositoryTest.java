package com.alinesno.cloud.platform.stack.repository;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.repository.OrderRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@SuppressWarnings("unused")
public class OrderRepositoryTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 

	@Autowired
	private OrderRepository orderRepository ; 
	
	@Test
	public void testFindAllByUserId() {
//		String userId = "2" ; 
//		Iterable<OrderEntity> list = orderRepository.findAllByUserId(userId) ; 
//		logger.debug("list = {}" , ToStringBuilder.reflectionToString(list));
	}
	
	@Test
	public void testFindAllByPage() {
		
	}

}
