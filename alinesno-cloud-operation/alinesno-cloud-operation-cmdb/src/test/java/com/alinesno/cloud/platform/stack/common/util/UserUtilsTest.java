package com.alinesno.cloud.platform.stack.common.util;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.operation.cmdb.common.util.UserUtils;
import com.alinesno.cloud.operation.cmdb.entity.UserEntity;

public class UserUtilsTest {

	private static final Logger logger = LoggerFactory.getLogger(UserUtilsTest.class) ; 
	
	@Test
	public void testRealName() {
	
		UserEntity u = new UserEntity() ;
		u.setRealName("罗小东");
		u.setPhone("15578942583");
		
		String name = UserUtils.realName(u) ; 
		logger.debug("name = {}" , name);
	}

}
