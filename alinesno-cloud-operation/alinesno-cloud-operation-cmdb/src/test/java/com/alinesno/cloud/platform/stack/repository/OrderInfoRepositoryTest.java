package com.alinesno.cloud.platform.stack.repository;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.OrderInfoEntity;
import com.alinesno.cloud.operation.cmdb.repository.OrderInfoRepository;


@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderInfoRepositoryTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 
	
	@Autowired
	private OrderInfoRepository orderInfoRepository ; 

	@Test
	public void testFindAllSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllPageable() {
		Page<OrderInfoEntity> listPage = orderInfoRepository.findAll(PageRequest.of(0, 10)) ; 

		logger.debug("list = {}" , listPage);
	}

}
