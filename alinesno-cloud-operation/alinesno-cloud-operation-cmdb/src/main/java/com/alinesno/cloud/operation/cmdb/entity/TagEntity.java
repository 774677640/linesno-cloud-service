package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 标签实体
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:09:14
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_tag")
public class TagEntity extends BaseEntity {

	private String tagName ; //标签名称
	private int tagOrder ; //标签排序
	
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public int getTagOrder() {
		return tagOrder;
	}
	public void setTagOrder(int tagOrder) {
		this.tagOrder = tagOrder;
	}

}
