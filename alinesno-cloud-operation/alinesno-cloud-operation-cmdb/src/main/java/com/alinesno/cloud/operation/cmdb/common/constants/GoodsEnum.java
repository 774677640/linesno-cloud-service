package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 订单状态
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum GoodsEnum {
	
	NOT_SALE("1", "下架"), // 人员
	SALE("0", "上架"); 

	public String code;
	public String text;

	GoodsEnum(String code, String text) {
		this.code = code;
		this.text = text;
	}

	public static GoodsEnum getStatus(String code) {
		if ("1".equals(code)) {
			return NOT_SALE;
		} else if ("0".equals(code)) {
			return SALE;
		} 
		return null;
	}

	public String getText() {
		return this.text;
	}

	public String getCode() {
		return this.code;
	}
}