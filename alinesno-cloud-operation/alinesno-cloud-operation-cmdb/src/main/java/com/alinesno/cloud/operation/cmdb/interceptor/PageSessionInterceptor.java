package com.alinesno.cloud.operation.cmdb.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.operation.cmdb.common.constants.RunConstants;
import com.alinesno.cloud.operation.cmdb.common.util.CookieUtils;
import com.alinesno.cloud.operation.cmdb.web.bean.ResponseBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResultGenerator;

/**
 * 页面拦截
 * 
 * @author LuoAnDong
 * @since 2018年8月24日 下午10:42:25
 */
//@Component
public class PageSessionInterceptor implements HandlerInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(ManagerSessionInterceptor.class);
	
	// 标示符：表示当前用户未登录(可根据自己项目需要改为json样式)
	ResponseBean<String> NO_LOGIN = ResultGenerator.genFailMessage("您还未登录") ;
	private static final String NO_LOGIN_URL = "/wx/auth/path" ;

	@Autowired
	private CookieUtils cookieUtils ; 

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		logger.debug("过滤微信页面权限.");
		
		HttpSession session = request.getSession();
		String fromWhere = cookieUtils.fullUrl(request);

		if (session != null && session.getAttribute(RunConstants.CURRENT_USER) != null) { // session中包含user对象,则是登录状态
			return true ; 
		} else {
			String requestType = request.getHeader("X-Requested-With");
			
			if(session != null) {
				session.setAttribute(RunConstants.FROM_WHERE, fromWhere);
				if (requestType != null && "XMLHttpRequest".equals(requestType)) { // 判断是否是ajax请求
					response.getWriter().write(JSONObject.toJSONString(NO_LOGIN));
					return false ; 
				} else {
					response.sendRedirect(request.getContextPath() + NO_LOGIN_URL); // 重定向到登录页(需要在static文件夹下建立此html文件)
					return false ; 
				}
			}
			
			response.sendRedirect(request.getContextPath() + NO_LOGIN_URL); // 重定向到登录页(需要在static文件夹下建立此html文件)
			return false ; 
		}
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// 初始化菜单
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
}
