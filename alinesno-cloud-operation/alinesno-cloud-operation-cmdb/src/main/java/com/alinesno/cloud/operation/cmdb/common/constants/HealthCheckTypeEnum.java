package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 健康状态检查 
 */
public enum HealthCheckTypeEnum {

	TCP("TCP") , //待发送
	HTTP("HTTP") ; //不再发送
	
	private String value ; 
	
	HealthCheckTypeEnum(String v) {
		this.value = v ; 
	}

	public String getValue() {
		return value;
	}
	
}
