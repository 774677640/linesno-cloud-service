package com.alinesno.cloud.operation.cmdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 启动入口
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
@EnableJpaAuditing
@SpringBootApplication
@EnableScheduling
@EnableAsync
@ServletComponentScan
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}
	
}
